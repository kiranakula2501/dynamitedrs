"""Automation Class: execute DAT30 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages


class VerifyCreateNewDM(unittest.TestCase):
    """Implementation of DAT30 testcase to verify Creating a new DM."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_create_new_dm_DAT30'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#db4c530'

    def test_verify_create_new_dm(self):
        """Testcase to verify the dm screen."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        self.page.write('=== Testcase Verify Create New DM Started at'
                        ' %s ===' % time.strftime('%H:%M:%S'))
        self.page.writeTableHeader(
            'DAT30 - Verify Create New DM')
        time.sleep(5)

        if self.page.check_element_present(
                self.current_bed.get("fab_button_launcher")) is True:
            is_present = self.page.check_element_present(
                self.current_bed.get("fab_button_launcher"))
            self.page.writeToTable('"FAB" button found at bottom right.')

        else:
            is_present = self.page.check_element_present(
                self.current_bed.get("fab_button"))
            self.page.writeToTable('"FAB" button found at top right.', None,
                                   None, self.error)

        if not is_present:
            self.page.writeToTable('"FAB" button is not found and exiting'
                                   ' the testacase', None, None, self.error)
            return False

        try:
            if self.page.check_element_present(
                    self.current_bed.get("fab_button_launcher")) is True:
                self.page.click_mobile_element(
                    self.current_bed.get("fab_button_launcher"))
            else:
                self.page.click_mobile_element(
                    self.current_bed.get("fab_button"))
            self.page.writeToTable('Successfully clicked on FAB button.')
        except NoSuchElementException:
            self.page.write('Click on FAB button is failed')
            self.page.writeToTable('Click on FAB button is failed', None, None,
                                   self.error)
            return False

        time.sleep(3)


        is_present = self.page.check_element_present(
            self.current_bed.get("search_nav_room"))
        if not is_present:
            self.page.writeToTable('"Find people  " search bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find people  " search bar is found.')

        try:
            self.page.set_mobile_text("sgudimitla@dynamitegroup.net",
                                      self.current_bed.get("search_nav_room"))
            self.page.write('Typed "sgudimitla@dynamitegroup.net"  text into search box ')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Find people  " search '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Find people  " '
                                   'search bar"', None, None, self.error)
            return False

        time.sleep(10)
        

        is_present = self.page.check_element_present(
            self.current_bed.get("click_account_1"))
        if not is_present:
            self.page.writeToTable('"click_account_1"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("click_account_1"))
                self.page.write('Successfully verified click_account_1')
                self.page.writeToTable('Successfully verified click_account_1')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding click_account_1 '
                                       , None, None,
                                       self.error)

        time.sleep(5)

        self.page.driver.hide_keyboard()


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout")
            first_image = self.golden_images + '/room_preview_layout.png'
            second_image = self.tc_dir_path + '/room_preview_layout.png'
            room_preview_layout = self.images.compare_images(
                first_image, second_image, 'room_preview_layout', self.tc_dir_path)

            score = float(round(room_preview_layout, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.')
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout')

        time.sleep(3)

        is_present = self.page.check_element_present(
            self.current_bed.get("compose_message"))
        if not is_present:
            self.page.writeToTable('"Compose message" bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Compose message"   bar is found.')

        try:
            self.page.set_mobile_text("Hey",
                                      self.current_bed.get("compose_message"))
            self.page.write('Typed "Hey"  text into compose box ')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Compose message  "   '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Compose message  " '
                                   ' box"', None, None, self.error)
            return False

        time.sleep(5)

        is_present = self.page.check_element_present(
            self.current_bed.get("post_button"))
        if not is_present:
            self.page.writeToTable('"post_button"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("post_button"))
                self.page.write('Successfully verified post_button')
                self.page.writeToTable('Successfully verified post_button')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding post_button '
                                       , None, None,
                                       self.error)

        time.sleep(5)



        self.page.write('=== DAT30 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' %
                        (int(time.time() - start_time)))
        self.page.writeTableFooter(' Test case DAT30 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyCreateNewDM('test_verify_create_new_dm'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
