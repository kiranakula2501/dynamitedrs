"""Automation Class: execute DAT12 testcase"""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException


class VerifyFilterRoomsDM(unittest.TestCase):
    """Implementation of DAT12 testcase to verify the filter rooms."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_filter_rooms_DAT12'
        self.images = ValidateImages()
        self.app_activity=conf.desired_caps['appActivity']
        self.app_package=conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_filter_rooms_dm(self):
        """Testcase to validate Verify filter rooms DM Get Started screen."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())

        self.page.write('=== Testcase DAT12 - Verify the Filter rooms/DM functionality'
                        ' from World view Get Started at %s ===' %
                        time.strftime("%H:%M:%S"))
        time.sleep(1)
        self.page.writeTableHeader('DAT12 -Verify the Filter rooms/DM'
                                   ' functionality from World view')


        self.scrol_up()

        time.sleep(1)

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable('"Find room or DM" search bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("Test",
                                      self.current_bed.get("search_box"))
            self.page.write('Typed "Test" example text into search box ')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Find room or DM" search '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Find room or DM" '
                                   'search bar"', None, None, self.error)
            return False
        
        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_test_keyword_filter_dat12")
            first_image = self.golden_images +\
                          '/verify_test_keyword_filter_dat12.png'
            second_image = self.tc_dir_path +\
                           '/verify_test_keyword_filter_dat12.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_test_keyword_filter_dat12', self.tc_dir_path)

            score = float(round(verify_login, 1))

            if score >= 1:
                self.page.write('Verify world view search box text screen '
                                ' verified successfully. '
                                'No major changes observed.')
                self.page.writeToTable('Verify world view search box text '
                                       ' screen verified successfully.'
                                       'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_test_keyword_filter_dat12' + 'Modified.png',)
            else:
                self.page.write('Verify world view search box text screen '
                                'verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable('Verify world view search box text '
                                       'screen verification failed'
                                       'Major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_test_keyword_filter_dat12' +
                                       'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images,
                'verify_test_keyword_filter_dat12')

        is_present = self.page.check_element_present(
            self.current_bed.get("timestamp_room"))

        if not is_present:
            self.page.writeToTable('"timestamp" is not found on the screen.',
                                   None, None, self.error)
        else:
            self.page.write('"timestamp" icon is found on the screen.')
            self.page.writeToTable('"timestamp" icon is found on the screen.')
        #self.page.driver.explicit_wait(self.current_bed.get("clear_filter"), 5)
        is_present = self.page.check_element_present(
            self.current_bed.get("clear_filter"))
        if not is_present:
            self.page.writeToTable('"Clear filter" is not found.', None, None,
                                   self.error)
        else:
            self.page.writeToTable('"Clear filter" is found on the screen.',
                                   self.error)

        try:
            self.page.click_mobile_element(self.current_bed.get("clear_filter"))
        except NoSuchElementException:
            self.page.write('Unable to click "Clear filter"')
            self.page.writeToTable('Unable to click "Clear filter"', None, None,
                                   self.error)
        self.scrol_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable('"Find room or DM" search bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("Star",
                                      self.current_bed.get("search_box"))
            self.page.write('Typed "Star" example text into search box')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Find room or DM" search '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Find room or DM" '
                                   'search bar"', None, None, self.error)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_room_keyword_filter_dat12")
            first_image = self.golden_images +\
                          '/verify_room_keyword_filter_dat12.png'
            second_image = self.tc_dir_path +\
                           '/verify_room_keyword_filter_dat12.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_room_keyword_filter_dat12',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write('Verify world view search box room name text '
                                'screen login screen verified successfully. '
                                'No major changes observed.')
                self.page.writeToTable('Verify world view search box room '
                                       'name text screen login screen verified'
                                       ' successfully. No major changes '
                                       'observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_room_keyword_filter_dat12' + 'Modified.png',)
            else:
                self.page.write('Verify world view search box room name text '
                                'screen verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable('Verify world view search box room '
                                       'name text screen login screen'
                                       ' verification failed. '
                                       'Major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_room_keyword_filter_dat12'
                                       + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images,
                'verify_room_keyword_filter_dat12')

        try:
            self.page.click_mobile_element(self.current_bed.get("clear_filter"))
        except NoSuchElementException:
            self.page.write('Unable to click "Clear filter"')
            self.page.writeToTable('Unable to click "Clear filter"', None, None,
                                   self.error)

        # ----------- Verification step 4 -----------------------
        self.scrol_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable('"Find room or DM" search bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("aStarredRoom",
                                      self.current_bed.get("search_box"))
            self.page.write('Typed "aStarredRoom" example text into search box')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Find room or DM" search '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Find room or DM" '
                                   'search bar"', None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("astarred_room"))
        if not is_present:
            self.page.writeToTable('"AstarredRoom" selected room is not found.',
                                   None, None, self.error)
        else:
            self.page.writeToTable('"AstarredRoom" selected room is found.')
        try:
            self.page.click_mobile_element(self.current_bed.get("starred_room"))
        except NoSuchElementException:
            self.page.write('Unable to click "AstarredRoom" selected room')
            self.page.writeToTable('Unable to click on "AstarredRoom" selected '
                                   'room', None, None, self.error)

        time.sleep(3)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_selected_room_screen_dat12")
            first_image = self.golden_images +\
                          '/verify_selected_room_screen_dat12.png'
            second_image = self.tc_dir_path +\
                           '/verify_selected_room_screen_dat12.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_selected_room_screen_dat12', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write('Verify selected room screen  '
                                ' verified successfully. '
                                'no major changes observed.')
                self.page.writeToTable('Verify selected room screen '
                                       'login screen verified successfully. '
                                       'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_selected_room_screen_dat12' + 'Modified.png',)
            else:
                self.page.write('Verify selected room screen '
                                'verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable('Verify selected room screen '
                                       'login screen verification failed. '
                                       'Major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_selected_room_screen_dat12'
                                       + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_selected_room_screen_dat12')

        # ----------- Verification step 4 End -----------------------

        self.page.driver.back()

        self.page.write('=== DAT12 ended at %s ===' % time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' %
                        (int(time.time() - start_time)))
        self.page.writeTableFooter(' Test case DAT12 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))
    def scrol_up(self):
        scroll_up = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_up[0], scroll_up[1], scroll_up[2],
                               scroll_up[3], scroll_up[4])
        time.sleep(2)

def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyFilterRoomsDM('test_verify_filter_rooms_dm'))
    return execute

if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
