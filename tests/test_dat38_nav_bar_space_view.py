"""Automation Class: execute DAT38 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException


class NavBarSpaceView(unittest.TestCase):
    """Implementation of DAT38 testcase to Verify the Nav bar
    functionality in space view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + \
                             '/verify_nav_bar_space_view_DAT38'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_nav_bar_space_view(self):
        """Testcase to Verify the Nav bar functionality in space view."""
        self.page.driver.start_activity(self.app_package, self.app_activity)
        start_time = int(time.time())
        time.sleep(1)

        self.page.write('=== Testcase DAT38 - Verify the Nav bar functionality'
                        ' in space view Get Started at %s ===' %
                        time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.scrol_down()
        self.page.writeTableHeader(
            'DAT38 - Nav bar functionality in space view')

        self.scrol_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("Dat38 Room",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering "Dat38" Room into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False
        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("dat38_room"))
        if not is_present:
            self.page.writeToTable(
                '"Dat38" Room is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"Dat38" Room is found.')
            self.page.write("DAT38 Room found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("dat38_room"))
        except NoSuchElementException:
            self.page.write('Unable to click on Dat38 Room')
            self.page.writeToTable('Unable to click on Dat38 Room',
                                   None, None, self.error)

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat38")
            first_image = self.golden_images + '/room_preview_layout_dat38.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat38.png'
            room_preview_layout_dat38 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat38', self.tc_dir_path)

            score = float(round(room_preview_layout_dat38, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/room_preview_layout_dat38' + 'Modified.png',)
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat38'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout_dat38')

        time.sleep(2)



        is_present = self.page.check_element_present(
            self.current_bed.get("navigate_back"))
        if not is_present:
            self.page.writeToTable(
                '"Navigate back Icon" name is not found.',
                None, None, self.error)
        else:
            self.page.write('"Navigate back Icon"  is found.')
            self.page.writeToTable('"Navigate back Icon"  is found.')


        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("num_of_membrs"))
        if not is_present:
            self.page.writeToTable(
                '"members in rooms are not found', None, None, self.error)
        else:
            result = self.page.get_text(self.current_bed.get("num_of_membrs"))
            self.page.write('Number of Members in room : '+ str(result))
            self.page.writeToTable('Number of Members in room : '+ str(result))

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("dm_details"))

        if not is_present:
            self.page.writeToTable('"dm_details" is not found and '
                                   'exiting all the testcases.',
                                   None, None, self.error)
            return False

        try:
            self.page.click_mobile_element(self.current_bed.get("dm_details"))
            self.page.write("dm_details has been found and clicked")
            self.page.writeToTable("dm_details has been found and clicked")
        except NoSuchElementException:
            self.page.write('Unable to find "dm_details"')
            self.page.writeToTable('Unable to find "dm_details" ,'
                                   ' exiting all the testcases.', None, None,
                                   self.error)

        is_present = self.page.check_element_present(
            self.current_bed.get("nav_up_button"))

        if not is_present:
            self.page.writeToTable('"nav_up_button" is not found and '
                                   'exiting all the testcases.',
                                   None, None, self.error)
            return False

        try:
            self.page.click_mobile_element(self.current_bed.get("nav_up_button"))
            self.page.write("nav_up_button has been found and clicked")
            self.page.writeToTable("nav_up_button has been found and clicked")
        except NoSuchElementException:
            self.page.write('Unable to find "nav_up_button"')
            self.page.writeToTable('Unable to find "nav_up_button" ,'
                                   ' exiting all the testcases.', None, None,
                                   self.error)


        is_present = self.page.check_element_present(
            self.current_bed.get("search_button"))
        if not is_present:
            self.page.writeToTable(
                '"Search Icon" is not found.', None, None, self.error)
        else:
            self.page.click_mobile_element(self.current_bed.get("search_button"))
            self.page.write('"Search Icon"  is found.')
            self.page.writeToTable('"Search Icon"  is found.')


        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("nav_up"))
        if not is_present:
            self.page.writeToTable(
                '"Navigate Back" is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("nav_up"))
        except NoSuchElementException:
            self.page.write(
                'Unable to click "Navigate Back"')
            self.page.writeToTable(
                'Unable to click "Navigate Back"',
                None, None, self.error)
            return False



        is_present = self.page.check_element_present(
            self.current_bed.get("nav_up_button"))
        if not is_present:
            self.page.writeToTable(
                '"Navigate Back" is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("nav_up_button"))
        except NoSuchElementException:
            self.page.write(
                'Unable to click "Navigate Back"')
            self.page.writeToTable(
                'Unable to click "Navigate Back"',
                None, None, self.error)
            return False



        is_present = self.page.check_element_present(
            self.current_bed.get("mendel_username"))
        if not is_present:
            self.page.writeToTable(
                '"Not taken back to World view ', None, None, self.error)
        else:
            self.page.write('Sucessfully Taken back to World view.')
            self.page.writeToTable('Sucessfully Taken back to World view.')

        self.page.write('=== DAT38 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT38 executed successfully in, %d Seconds'
            % (int(time.time() - start_time)))


    def go_back(self, rotate):
        """ function to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for _ in range(rotate):
            self.page.driver.back()

    def scrol_up(self):
        """method to pull up the screen"""
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)

    def scrol_down(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)



def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(NavBarSpaceView('test_nav_bar_space_view'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
