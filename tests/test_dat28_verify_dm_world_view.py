"""Automation Class: execute DAT28 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from appium.webdriver.common.touch_action import TouchAction

class VerifyDMWorldView(unittest.TestCase):
    """Implementation of DAT28 testcase to verify Creating a new room."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_dm_world_view_DAT28'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_dm_world_view(self):
        """Testcase to verify the dm screen."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        self.page.write('=== Testcase DAT28 - Verify DM World View Started at'
                        ' %s ===' % time.strftime('%H:%M:%S'))
        self.page.writeTableHeader(
            'DAT28 - Verify DM World View Room')
        time.sleep(1)

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        self.page.driver.hide_keyboard()

        is_present = self.page.check_element_present(
            self.current_bed.get("group_message"))
        if not is_present:
            self.page.writeToTable('"group_message"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified group_message')
                self.page.writeToTable('Successfully verified group_message')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding group_message '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("message_bot"))
        if not is_present:
            self.page.writeToTable('"message_bot"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified message_bot')
                self.page.writeToTable('Successfully verified message_bot')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding message_bot '
                                       , None, None,
                                       self.error)

        time.sleep(2)


        is_present = self.page.check_element_present(
            self.current_bed.get("create_room"))
        if not is_present:
            self.page.writeToTable('"create_room"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified create_room')
                self.page.writeToTable('Successfully verified create_room')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding create_room '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable('"browse_rooms"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified browse_rooms')
                self.page.writeToTable('Successfully verified browse_rooms')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding browse_rooms '
                                       , None, None,
                                       self.error)

        time.sleep(2)


        is_present = self.page.check_element_present(
            self.current_bed.get("frequent"))
        if not is_present:
            self.page.writeToTable('"frequent"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified frequent')
                self.page.writeToTable('Successfully verified frequent')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding frequent '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat28")
            first_image = self.golden_images + '/room_preview_layout_dat28.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat28.png'
            room_preview_layout_dat28 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat28', self.tc_dir_path)

            score = float(round(room_preview_layout_dat28, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat28'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat28'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout_dat28')

        time.sleep(2)
        self.page.write('=== DAT28 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' %
                        (int(time.time() - start_time)))
        self.page.writeTableFooter(' Test case DAT28 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyDMWorldView('test_verify_dm_world_view'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
