"""Automation Class: execute DAT111 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction



class VerifyBotOptionFab(unittest.TestCase):
    """Implementation of DAT111 testcase to Verify the Bot option
     displayed in FAB of World view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_bot_option_fab_DAT111'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_bot_option_fab(self):

        """Testcase to Verify the Bot option displayed in FAB of World view"""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        time.sleep(1)

        self.page.write(
            '=== Testcase Verify Bot Option Fab world view Started at %s ===' %
            time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader(
            'DAT111 - Verify the Bot option displayed in FAB of World view')

        self.page.write('Checking "FAB Button" is available or not.')
        self.page.writeToTable(
            'Checking "FAB Button"  is available or not.')

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        self.page.driver.hide_keyboard()


        self.page.write(
            'Checking "Group Message" is available or not.')
        self.page.writeToTable(
            'Checking "Group Message" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("group_message"))
        if not is_present:
            self.page.writeToTable(
                '"Group Message" name is not found.', None, None, self.error)
        else:
            self.page.write('"Group Message" name is found.')
            self.page.writeToTable('"Group Message" name is found.')

        self.page.write(
            'Checking "Message a bot" is available or not.')
        self.page.writeToTable(
            'Checking "Message a bot" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("message_bot"))
        if not is_present:
            self.page.writeToTable(
                '"Message a bot" name is not found.', None, None, self.error)
        else:
            self.page.write('"Message a bot" name is found.')
            self.page.writeToTable('"Message a bot" name is found.')

        self.page.write(
            'Checking "Create and browse room" is available or not.')
        self.page.writeToTable(
            'Checking "Create or browse room" is available or not.')


        is_present = self.page.check_element_present(
            self.current_bed.get("create_room"))
        if not is_present:
            self.page.writeToTable(
                '"Create room"  is not found.',
                None, None, self.error)
        else:
            self.page.write('"Create room"  is found.')
            self.page.writeToTable('"Create room"  is found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable(
                '"browse room"  is not found.',
                None, None, self.error)
        else:
            self.page.write('"browse room"  is found.')
            self.page.writeToTable('"browse room"  is found.')



        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_find_people_screen")
            first_image = self.golden_images + '/verify_find_people_screen.png'
            second_image = self.tc_dir_path + '/verify_find_people_screen.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_find_people_screen', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify Find people screen verfied successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify Find people screen verfied successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/verify_find_people_screen'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Verify Find people screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify Find people screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/verify_find_people_screen'
                    + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_find_people_screen')

        self.page.write(
            'Checking "Message a bot" is available or not.')
        self.page.writeToTable(
            'Checking "Message a bot" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("message_bot"))
        if not is_present:
            self.page.writeToTable(
                '"Message a bot" screen is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("message_bot"))
        except NoSuchElementException:
            self.page.write(
                'Unable to click "Message a bot" screen ')
            self.page.writeToTable(
                'Unable to click "Message a bot" screen ',
                None, None, self.error)
            return False


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_add_bot_screen")
            first_image = self.golden_images + '/verify_add_bot_screen.png'
            second_image = self.tc_dir_path + '/verify_add_bot_screen.png'
            verify_login = self.images.compare_images(
                first_image, second_image, 'verify_add_bot_screen',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write('Verify Add bot screen verfied successfully. '
                                'no major changes observed.')
                self.page.writeToTable(
                    'Verify Add bot screen verfied successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/verify_add_bot_screen'
                    + 'Modified.png')
            else:
                self.page.write('Verify Add bot screen verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify Add bot screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/verify_add_bot_screen'
                    + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_add_bot_screen')

        self.page.write(
            'Checking "Add bot" screen is available or not.')
        self.page.writeToTable(
            'Checking "Add bot" screen is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("add_bot_screen"))
        if not is_present:
            self.page.writeToTable(
                '"Add bot" screen is not found.',
                None, None, self.error)
        else:
            self.page.write('"Add bot" screen is found.')
            self.page.writeToTable('"Add bot" screen is found.')

        self.page.write(
            'Checking "Navigate Back" is available or not.')
        self.page.writeToTable(
            'Checking "Navigate Back" button is available or not.')

        is_present = self.page.check_element_present(
            self.current_bed.get("navigate_back"))
        if not is_present:
            self.page.writeToTable(
                '"Navigate Back" is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("navigate_back"))
            self.page.write(
                'Clicked "Navigate Back" button')
            self.page.writeToTable(
                'Clicked "Navigate Back" button')
        except NoSuchElementException:
            self.page.write(
                'Unable to click "Navigate Back"')
            self.page.writeToTable(
                'Unable to click "Navigate Back"',
                None, None, self.error)
            return False
        time.sleep(2)

        self.page.write('=== DAT111 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT111 executed successfully in, %d Seconds' %
            (int(time.time() - start_time)))

def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(VerifyBotOptionFab('test_verify_bot_option_fab'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
