"""Automation Class: execute DAT7 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages


class VerifyWorldViewElements(unittest.TestCase):
    """Implementation of DAT7 testcase to verify the elements presence of
    worldview."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/elements_world_view_DAT7'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'


    def test_verify_elements_world_view(self):
        """Testcase to verify the elements presence in  world view."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())

        self.page.write('=== Testcase DAT7 - Verify Elements World View Started at'
                        ' %s ===' % time.strftime('%H:%M:%S'))
        self.page.writeTableHeader(
            'DAT7 - Verify the UI Sections/Elements displayed in world view')
        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("mendel_bar"))
        if not is_present:
            self.page.writeToTable('Mendel bar is not found.',
                                   None, None, self.error)
            self.page.write('Mendel bar is not found')
        else:
            self.page.write('Mendel bar is found on the screen.')
            self.page.writeToTable('Mendel bar is found on the screen.')

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("mendel_username"))
        if not is_present:
            self.page.writeToTable('Mendel username is not found.',
                                   None, None, self.error)
            self.page.write('Mendel username is not found')
        else:
            self.page.write('Mendel username is found on the screen.')
            self.page.writeToTable('Mendel username is found on the screen.')

        time.sleep(2)
        

        is_present = self.page.check_element_present(
            self.current_bed.get("presence_information"))
        if not is_present:
            self.page.writeToTable('Presence Information is not found.',
                                   None, None, self.error)
            self.page.write('Presence Information is not found')
        else:
            self.page.write('Presence Information is found on the screen.')
            self.page.writeToTable('Presence Information is found on the screen.')

        time.sleep(2)
        
        if self.page.check_element_present(
                self.current_bed.get("fab_button_launcher")) is True:

            is_present = self.page.check_element_present(
                self.current_bed.get("fab_button"))
        else:

            is_present = self.page.check_element_present(
                self.current_bed.get("fab_button"))

        if not is_present:
            self.page.writeToTable('"FAB Button" is not found.', self.error)
        else:
            self.page.write('"FAB Button" is available.')
            self.page.writeToTable('"FAB Button" is found on the screen.')

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("presence_indicator"))
        if not is_present:
            self.page.writeToTable('Presence indicator is not found.',
                                   None, None, self.error)
        else:
            self.page.write('Presence indicator is available.')
            self.page.writeToTable('Presence indicator is available on '
                                   'the screen.')


        is_present = self.page.check_element_present(
            self.current_bed.get("unread_section"))

        if not is_present:
            self.page.writeToTable('Unread section is not found.',
                                   self.error)
        else:
            self.page.write('Unread section is available.')
            self.page.writeToTable('Unread section is available on the '
                                   'screen.')

        is_present = self.page.check_element_present(
            self.current_bed.get("badge_count"))

        if not is_present:
            self.page.writeToTable('Badge count is not found.',
                                   self.error)
        else:
            self.page.write('Badge count is available.')
            self.page.writeToTable('Badge count is available on the '
                                   'screen.')


        is_present = self.page.check_element_present(
            self.current_bed.get("mute_icon"))

        if not is_present:
            self.page.writeToTable('Mute_icon is not found.',
                                   self.error)
        else:
            self.page.write('Mute_icon is available.')
            self.page.writeToTable('Mute_icon is available on the '
                                   'screen.')
            
        is_present = self.page.check_element_present(
            self.current_bed.get("recent_section"))

        if not is_present:
            self.page.writeToTable('Recent section is not found.',
                                   self.error)
        else:
            self.page.write('Recent section is available.')
            self.page.writeToTable('Recent section is available on the '
                                   'screen.')
            
        is_present = self.page.check_element_present(
            self.current_bed.get("starred_section"))

        if not is_present:
            self.page.writeToTable('Starred section is not found.',
                                   self.error)
        else:
            self.page.write('Starred section is available.')
            self.page.writeToTable('Starred section is available on the '
                                   'screen.')

        self.scrol_down()
            
        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("world_view_layout_dat7")
            first_image = self.golden_images + '/world_view_layout_dat7.png'
            second_image = self.tc_dir_path + '/world_view_layout_dat7.png'
            onboard_message = self.images.compare_images(
                first_image, second_image, 'world_view_layout_dat7',
                self.tc_dir_path)

            score = float(round(onboard_message, 1))
            if score >= 1:
                self.page.write('World view layout verified successfully. '
                                'no major changes observed.')
                self.page.writeToTable('World view layout verified'
                                       ' successfully. No major changes '
                                       'observed.', first_image,
                                       self.tc_dir_path + '/world_view_layout_dat7'
                                       + 'Modified.png')
            else:
                self.page.write('World view layout verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable('World view layout verification failed. '
                                       'Major changes observed.', first_image,
                                       self.tc_dir_path + '/world_view_layout_dat7'
                                       + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(self.golden_images,
                                              'world_view_layout_dat7')

        self.page.write('=== DAT7 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' %
                        (int(time.time() - start_time)))
        self.page.writeTableFooter(' Test case DAT7 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()

    def scrol_down(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyWorldViewElements('test_verify_elements_world_view'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
