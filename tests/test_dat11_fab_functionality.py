"""Automation Class: execute DAT11 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction


class VerifyFABbutton(unittest.TestCase):
    """Implementation of DAT11 testcase to verify the elements of fab"""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + conf.desired_caps.get(
            'deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_FAB_button_DAT11'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_FAB_button(self):
        """Testcase to validate World View Starred Rooms Get Started screen."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())

        self.page.write('=== Testcase DAT11 - Verify the FAB button functionality'
                        ' displayed in World view at %s ===' %
                        time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader('DAT11 - Verify the FAB button functionality'
                                   ' displayed in World view')


        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        self.page.driver.hide_keyboard()

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("group_message"))
        if not is_present:
            self.page.write('Group Message is not found')
            self.page.writeToTable('"Group Message" option is not found.',
                                   None, None, self.error)
        else:
            self.page.write('"Group Message" option is present on the '
                                   'screen.')
            self.page.writeToTable('"Group Message" option is present on the '
                                   'screen.')

        is_present = self.page.check_element_present(
            self.current_bed.get("message_bot"))
        if not is_present:
            self.page.writeToTable('"Message a bot" is not found '
                                   'on the screen', None, None, self.error)
        else:
            self.page.writeToTable('"Message a bot" option is found '
                                   'on the screen')


        is_present = self.page.check_element_present(
            self.current_bed.get("create_room"))
        if not is_present:
            self.page.writeToTable('"Create room" option is not found.',
                                   None, None, self.error)
        else:
            self.page.write('"Create room" option is available.')
            self.page.writeToTable('"Create room" option is available.')

        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable('"browse rooms" option is not found.',
                                   None, None, self.error)
        else:
            self.page.write('"browse rooms" option is available.')
            self.page.writeToTable('"browse rooms" option is available.')

        is_present = self.page.check_element_present(
            self.current_bed.get("frequent"))
        if not is_present:
            self.page.writeToTable('"frequent section" bar is not found.',
                                   None, None, self.error)
        else:
            self.page.write('"frequent section" bar found on the screen.')
            self.page.writeToTable('"frequent section" bar found on the '
                                   'screen.')

        if conf.execution_type == 'automation': 
            self.page.save_screenshot_png("verify_FAB_screen_dat11")
            first_image = self.golden_images + '/verify_FAB_screen_dat11.png'
            second_image = self.tc_dir_path + '/verify_FAB_screen_dat11.png'
            verify_fab = self.images.compare_images(
                first_image, second_image, 'verify_FAB_screen_dat11', self.tc_dir_path)
    
            score = float(round(verify_fab, 1))
            if score >= 1:
                self.page.write('FAB screen verfied successfully. No major '
                                'changes observed.')
                self.page.writeToTable('FAB screen verfied successfully.'
                                       ' No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_FAB_screen_dat11' + 'Modified.png',)
            else:
                self.page.write('FAB screen verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable('FAB Screen verification failed. Major '
                                       'changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_FAB_screen_dat11' + 'Modified.png',
                                       self.error)
        else:
            self.page.golden_image_screenshot(self.golden_images,
                                              'verify_FAB_screen_dat11')


        self.page.driver.back()

        self.page.write('=== DAT11 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(' Test case DAT11 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyFABbutton('test_verify_FAB_button'))
    return execute


if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
