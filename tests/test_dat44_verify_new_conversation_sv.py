"""Automation Class: execute DAT44 testcase"""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException



class VerifyNewConversationSV(unittest.TestCase):
    """Implementation of DAT44 testcase to Verify the new conversation
     FAB in space view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_new_conversation_sv_DAT44'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_new_conversation_sv(self):

        """Testcase to Verify the new conversation
        FAB in space view"""
        self.page.driver.start_activity(self.app_package,
                                        self.app_activity)
        start_time = int(time.time())
        time.sleep(1)

        self.page.write(
            '=== Testcase DAT44 - Verify the new conversation FAB in space view '
            'Started at %s ===' %time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader(
            'DAT44 - Verify the new conversation FAB in space view ')

        print 'app activity launching'
        self.page.driver.start_activity(self.app_package,
                                        self.app_activity)
        time.sleep(2)
        self.pull_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("Dat44",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering "Dat44" Room into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False


        is_present = self.page.check_element_present(
            self.current_bed.get("dat44_room"))
        if not is_present:
            self.page.writeToTable(
                '"Dat44" Room is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"Dat41" Room is found.')
        try:
            self.page.click_mobile_element(
                self.current_bed.get("dat44_room"))
        except NoSuchElementException:
            self.page.write('Unable to click on Dat44 Room')
            self.page.writeToTable('Unable to click on Dat44 Room',
                                   None, None, self.error)

        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat44")
            first_image = self.golden_images + '/room_preview_layout_dat44.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat44.png'
            room_preview_layout_dat44 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat44', self.tc_dir_path)

            score = float(round(room_preview_layout_dat44, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat44'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat44'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout_dat44')


        if self.page.check_element_present(
                self.current_bed.get("new_topic")) is True:
            is_present = self.page.check_element_present(
                self.current_bed.get("new_topic"))
        else:
            is_present = self.page.check_element_present(
                self.current_bed.get("new_topic_menu"))

        if not is_present:
            self.page.writeToTable('"New Topic" is not found.',
                                   None, None, self.error)
            return False

        try:
            if self.page.check_element_present(
                    self.current_bed.get("new_topic")) is True:
                self.page.write('Clicking on New Topic')
                self.page.click_mobile_element(
                    self.current_bed.get("new_topic"))
            else:
                self.page.write('Clicking on New Topic')
                self.page.click_mobile_element(
                    self.current_bed.get("new_topic_menu"))
        except NoSuchElementException:
            self.page.write('Unable to click New Topic')
            self.page.writeToTable('Unable to click New Topic',
                                   None, None, self.error)
            return False

        self.page.driver.hide_keyboard()

        is_present = self.page.check_element_present(
            self.current_bed.get("new_conversation"))
        if not is_present:
            self.page.writeToTable(
                '"New conversation" window is not found.',
                None, None, self.error)
        else:
            self.page.write('"New conversation" window is found.')
            self.page.writeToTable('"New conversation" window is found.')

        self.page.driver.back()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_button"))
        if not is_present:
            self.page.writeToTable(
                '"Not taken back to space view ', None, None, self.error)
        else:
            self.page.write('Taken back to space view.')
            self.page.writeToTable('Taken back to space view.')

        self.page.driver.back()


        self.page.write('=== DAT44 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT44 executed successfully in, %d Seconds'
            % (int(time.time() - start_time)))



    def pull_up(self):
        """Method to pull up the screen"""
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)



def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(VerifyNewConversationSV('test_verify_new_conversation_sv'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
