"""Automation Class: execute DAT78 test case"""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException



class VerifySpaceSearch(unittest.TestCase):
    """Implementation of DAT78 testcase to verify space search."""
    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_space_search_DAT78'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'


    def test_verify_space_search(self):

        self.page.driver.start_activity(self.app_package, self.app_activity)


        """Testcase to validate search room preview screen."""
        try:
            self.page.driver.start_activity(self.app_package, self.app_activity)
            start_time = int(time.time())

            self.page.write('=== Testcase DAT78 Verify space search Started at %s ===' %
                            time.strftime("%H:%M:%S"))

            self.page.writeTableHeader(
                'DAT78 - Verify Space Search Testcase')
            time.sleep(1)
            self.scrol_up()

            is_present = self.page.check_element_present(
                self.current_bed.get("search_box"))
            if not is_present:
                self.page.writeToTable(
                    '"Find room or DM" search bar is not found.',
                    None, None, self.error)
                return False
            else:
                self.page.writeToTable('"Find room or DM" search bar is found.')

            try:
                self.page.set_mobile_text("Dat78 Room",
                                          self.current_bed.get("search_box"))
                self.page.write('Entering "Dat78" Room into search box ')
            except NoSuchElementException:
                self.page.write(
                    'Unable to input text in "Find room or DM" search '
                    'bar"')
                self.page.writeToTable(
                    'Unable to input text in "Find room or DM" '
                    'search bar"', None, None, self.error)
                return False
            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("dat78_room"))
            if not is_present:
                self.page.writeToTable(
                    '"Dat78" Room is not found.',
                    None, None, self.error)
            else:
                self.page.writeToTable('"Dat78" Room is found.')
                self.page.write("DAT78 Room found")
            try:
                self.page.click_mobile_element(
                    self.current_bed.get("dat78_room"))
            except NoSuchElementException:
                self.page.write('Unable to click on Dat78 Room')
                self.page.writeToTable('Unable to click on Dat78 Room',
                                       None, None, self.error)

            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("search_room"))
            if not is_present:
                self.page.writeToTable(
                    '"search" Room is not found.',
                    None, None, self.error)
            else:
                self.page.writeToTable('"search" Room is found.')
                self.page.write("search Room found")
            try:
                self.page.click_mobile_element(
                    self.current_bed.get("search_room"))
            except NoSuchElementException:
                self.page.write('Unable to click on search_room')
                self.page.writeToTable('Unable to click on search_room',
                                       None, None, self.error)

            time.sleep(2)

            self.page.driver.hide_keyboard()

            time.sleep(2)

            self.page.write('Verifying search bar preview')

            if conf.execution_type == 'automation':
                self.page.save_screenshot_png("search_bar_layout_DAT78")
                first_image = self.golden_images + '/search_bar_layout_DAT78.png'
                second_image = self.tc_dir_path + '/search_bar_layout_DAT78.png'
                search_bar_layout_DAT78 = self.images.compare_images(
                    first_image, second_image, 'search_bar_layout_DAT78', self.tc_dir_path)

                score = float(round(search_bar_layout_DAT78, 1))
                if score >= 1:
                    self.page.write(
                        'Search Bar Preview Layout screen verfied succsfully. '
                        'no major changes observed.')
                    self.page.writeToTable(
                        'Search bar Preview Layout screen verfied succsfully. '
                        'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/search_bar_layout_DAT78' + 'Modified.png',)
                else:
                    self.page.write(
                        'Search Bar Preview Layout screen verification failed. '
                        'Major changes observed.', 'CRITICAL')
                    self.page.writeToTable(
                        'Search Bar Layout screen verification failed. '
                        'Major changes observed.', first_image,
                        self.tc_dir_path + '/search_bar_layout_DAT78'
                        + 'Modified.png', self.error)
            else:
                self.page.golden_image_screenshot(
                    self.golden_images, 'search_bar_layout_DAT78')

            is_present = self.page.check_element_present(
                self.current_bed.get("search_back"))

            if not is_present:
                self.page.writeToTable('"search_back" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("search_back has been found")
                self.page.writeToTable("search_back has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "search_back"')
                self.page.writeToTable('Unable to find "search_back" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)

            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("search_nav_room"))

            if not is_present:
                self.page.writeToTable('"search_nav_room" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("search_nav_room has been found")
                self.page.writeToTable("search_nav_room has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "search_nav_room"')
                self.page.writeToTable('Unable to find "search_nav_room" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("user_name"))

            if not is_present:
                self.page.writeToTable('"Members" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("Members have been found")
                self.page.writeToTable("members has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "members"')
                self.page.writeToTable('Unable to find "members" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("user_avatar"))

            if not is_present:
                self.page.writeToTable('"user_avatar" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("user_avatar has been found")
                self.page.writeToTable("user_avatar has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "user_avatar"')
                self.page.writeToTable('Unable to find "user_avatar" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("separator_searchnav"))

            if not is_present:
                self.page.writeToTable('"separator_searchnav" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("separator_searchnav has been found")
                self.page.writeToTable("separator_searchnav has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "separator_searchnav"')
                self.page.writeToTable('Unable to find "separator_searchnav" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)



            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("content_filter"))

            if not is_present:
                self.page.writeToTable('"content_filter" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("content_filter has been found")
                self.page.writeToTable("content_filter has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "content_filter"')
                self.page.writeToTable('Unable to find "content_filter" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("me"))

            if not is_present:
                self.page.writeToTable('"me" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("me has been found")
                self.page.writeToTable("me has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "me"')
                self.page.writeToTable('Unable to find "me" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("photos"))

            if not is_present:
                self.page.writeToTable('"photos" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("photos has been found")
                self.page.writeToTable("photos has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "photos"')
                self.page.writeToTable('Unable to find "photos" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)



            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("videos"))

            if not is_present:
                self.page.writeToTable('"videos" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("videos has been found")
                self.page.writeToTable("videos has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "videos"')
                self.page.writeToTable('Unable to find "videos" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)




            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("links"))

            if not is_present:
                self.page.writeToTable('"links" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("links has been found")
                self.page.writeToTable("photos has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "links"')
                self.page.writeToTable('Unable to find "links" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("docs"))

            if not is_present:
                self.page.writeToTable('"docs" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("docs has been found")
                self.page.writeToTable("docs has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "docs"')
                self.page.writeToTable('Unable to find "docs" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)

            self.scrol_right()

            is_present = self.page.check_element_present(
                self.current_bed.get("slides"))

            if not is_present:
                self.page.writeToTable('"slides" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("slides has been found")
                self.page.writeToTable("slides has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "slides"')
                self.page.writeToTable('Unable to find "slides" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("sheets"))

            if not is_present:
                self.page.writeToTable('"sheets" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("sheets has been found")
                self.page.writeToTable("sheets has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "sheets"')
                self.page.writeToTable('Unable to find "sheets" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("pdfs"))

            if not is_present:
                self.page.writeToTable('"pdfs" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("pdfs has been found")
                self.page.writeToTable("pdfs has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "pdfs"')
                self.page.writeToTable('Unable to find "pdfs" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("all_files"))

            if not is_present:
                self.page.writeToTable('"all_files" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("all_files has been found")
                self.page.writeToTable("all_files has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "all_files"')
                self.page.writeToTable('Unable to find "all_files" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)


            self.page.write('=== Rendering DAT78 ended at %s ===' %
                            time.strftime("%H:%M:%S"))
            self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                                   - start_time)))
            self.page.writeTableFooter(' Test case DAT78 executed successfully in, %d Seconds' % (int(time.time()
                                                                   - start_time)))
        except Exception,e:
            subprocess.Popen('adb shell am force-stop "'+self.app_package+'"', shell=True)
            time.sleep(2)
            subprocess.Popen('adb shell am start -n "'+self.app_package+'/'
                         +self.app_activity+'"', shell=True)
            print "Exception :",str(e)
            time.sleep(2)


    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


    def scrol_up(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)

    def scrol_right(self):
        scroll_right = self.current_bed.get("swipe_right")
        self.page.write('Screen scrolling to right')
        self.page.driver.swipe(scroll_right[0], scroll_right[1], scroll_right[2],
                               scroll_right[3], scroll_right[4])
        time.sleep(2)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(VerifySpaceSearch('test_verify_space_search'))
    return suite


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
