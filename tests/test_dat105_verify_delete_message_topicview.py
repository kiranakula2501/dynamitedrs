"""Automation Class: execute DAT105 test case."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from appium.webdriver.common.touch_action import TouchAction
from selenium.common.exceptions import NoSuchElementException


class VerifyDeleteMessageTopicView(unittest.TestCase):
    """Implementation of DAT105 testcase to verify delete message in topic
     view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root +\
                             '/verify_delete_message_topicview_DAT105'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'
        self.action = TouchAction(conf.driver)

    def test_verify_delete_message_topicview(self):

        """Testcase to verify delete message in topic view."""
        try:
            self.page.driver.start_activity(self.app_package, self.app_activity)
            start_time = int(time.time())
            time.sleep(1)

            self.page.write('=== Testcase Verify Delete Message Topic View Started at %s ===' %
                            time.strftime("%H:%M:%S"))
            time.sleep(1)
            self.page.writeTableHeader(
                'DAT105 - Verify Delete Message Topic View Testcase')

            self.scrol_up()

            is_present = self.page.check_element_present(
                self.current_bed.get("search_box"))
            if not is_present:
                self.page.writeToTable(
                    '"Find room or DM" search bar is not found.',
                    None, None, self.error)
                return False
            else:
                self.page.writeToTable('"Find room or DM" search bar is found.')

            try:
                self.page.set_mobile_text("Dat105 Room",
                                          self.current_bed.get("search_box"))
                self.page.write('Entering "Dat105" Room into search box ')
            except NoSuchElementException:
                self.page.write(
                    'Unable to input text in "Find room or DM" search '
                    'bar"')
                self.page.writeToTable(
                    'Unable to input text in "Find room or DM" '
                    'search bar"', None, None, self.error)
                return False
            time.sleep(2)


            is_present = self.page.check_element_present(
                self.current_bed.get("dat105_room"))
            if not is_present:
                self.page.writeToTable(
                    '"Dat105" Room is not found.',
                    None, None, self.error)
            else:
                self.page.writeToTable('"Dat105" Room is found.')
                self.page.write("DAT105 Room found")
            try:
                self.page.click_mobile_element(
                    self.current_bed.get("dat105_room"))
            except NoSuchElementException:
                self.page.write('Unable to click on Dat105 Room')
                self.page.writeToTable('Unable to click on Dat105 Room',
                                       None, None, self.error)

            time.sleep(3)

            self.page.click_mobile_element(self.current_bed.get(
                "new_topic"))


            self.page.set_mobile_text("Reply text1",
                                      self.current_bed.get("hint_text"))

            self.page.click_mobile_element(self.current_bed.get('post_message'))

            time.sleep(2)

            self.go_back(1)

            is_present = self.page.check_element_present(
                self.current_bed.get("reply_to_topic"))

            if not is_present:
                self.page.writeToTable('"Reply topic" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.click_mobile_element(self.current_bed.get(
                    "reply_to_topic"))
                self.page.write("Clicking on reply to topic")
                self.page.writeToTable("Clicking on reply to topic")
            except NoSuchElementException:
                self.page.write('Unable to click "reply_to_topic"')
                self.page.writeToTable('Unable to click "reply_to_topic" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)

            self.page.driver.hide_keyboard()


            self.page.write('Long press on message')
            self.page.writeToTable('Long press on message')
            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("get_message"))

            if not is_present:
                self.page.writeToTable('"Message" is not found and '
                                       'exiting the testcase.',
                                       None, None, self.error)

            try:
                get_message = self.page.driver.find_element_by_xpath(
                    self.current_bed.get("get_message"))
                self.action.press(get_message).wait(3000).release().perform()
                self.page.write("Long pressing on message")
                self.page.writeToTable("Long pressing on message")

            except NoSuchElementException:
                self.page.write('Unable to long press "message"')
                self.page.writeToTable('Unable to long press "message" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
            time.sleep(2)


            self.page.write('Verifying topic reply preview')

            if conf.execution_type == 'automation':
                self.page.save_screenshot_png("long_press_layout_dat105")
                first_image = self.golden_images + '/long_press_layout_dat105.png'
                second_image = self.tc_dir_path + '/long_press_layout_dat105.png'
                long_press_layout_dat105 = self.images.compare_images(
                    first_image, second_image, 'long_press_layout_dat105', self.tc_dir_path)

                score = float(round(long_press_layout_dat105, 1))
                if score >= 1:
                    self.page.write('long_press_layout_dat105 screen verfied succsfully. '
                                    'no major changes observed.')
                    self.page.writeToTable('long_press_layout_dat105 screen verfied succsfully. '
                                           'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/long_press_layout_dat105' + 'Modified.png',)
                else:
                    self.page.write('long_press_layout_dat105 screen verification failed. '
                                    'Major changes observed.', 'CRITICAL')
                    self.page.writeToTable('long_press_layout_dat105 screen verification failed. '
                                           'Major changes observed.', first_image,
                                           self.tc_dir_path + '/long_press_layout_dat105' + 'Modified.png', self.error)

            is_present = self.page.check_element_present(
                self.current_bed.get("copy_text"))

            if not is_present:
                self.page.writeToTable('"copy_text" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("copy_text has been found")
                self.page.writeToTable("copy_text has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "copy_text"')
                self.page.writeToTable('Unable to find "copy_text" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)

            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("edit_option"))

            if not is_present:
                self.page.writeToTable('"edit_option" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("edit_option has been found")
                self.page.writeToTable("edit_option has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "edit_option"')
                self.page.writeToTable('Unable to find "edit_option" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("delete_option"))

            if not is_present:
                self.page.writeToTable('"delete_option" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.click_mobile_element(self.current_bed.get("delete_option"))
                self.page.write("delete_option has been found and clicked")
                self.page.writeToTable("delete_option has been found and clicked")
            except NoSuchElementException:
                self.page.write('Unable to find "edit_option"')
                self.page.writeToTable('Unable to find "edit_option" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)



            time.sleep(2)



            if conf.execution_type == 'automation':
                self.page.save_screenshot_png("delete_press_layout_dat105")
                first_image = self.golden_images + '/delete_press_layout_dat105.png'
                second_image = self.tc_dir_path + '/delete_press_layout_dat105.png'
                delete_press_layout_dat105 = self.images.compare_images(
                    first_image, second_image, 'delete_press_layout_dat105', self.tc_dir_path)

                score = float(round(delete_press_layout_dat105, 1))
                if score >= 1:
                    self.page.write('delete_press_layout_dat105 screen verfied succsfully. '
                                    'no major changes observed.')
                    self.page.writeToTable('delete_press_layout_dat105 screen verfied succsfully. '
                                           'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/delete_press_layout_dat105' + 'Modified.png',)
                else:
                    self.page.write('delete_press_layout_dat105 screen verification failed. '
                                    'Major changes observed.', 'CRITICAL')
                    self.page.writeToTable('delete_press_layout_dat105 screen verification failed. '
                                           'Major changes observed.', first_image,
                                           self.tc_dir_path + '/delete_press_layout_dat105' + 'Modified.png', self.error)
            is_present = self.page.check_element_present(
                self.current_bed.get("cancel_button"))

            if not is_present:
                self.page.writeToTable('"cancel_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.click_mobile_element(self.current_bed.get("cancel_button"))
                self.page.write("cancel_button has been found and clicked")
                self.page.writeToTable("cancel_button has been found and clicked")
            except NoSuchElementException:
                self.page.write('Unable to find "cancel_button"')
                self.page.writeToTable('Unable to find "cancel_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)

            self.page.write('Long press on message')
            self.page.writeToTable('Long press on message')
            get_message = self.page.driver.find_element_by_xpath(
                "//android.widget.TextView[contains(@text,'Reply text1')]")
            is_present = self.page.check_element_present(
                self.current_bed.get("get_message"))

            if not is_present:
                self.page.writeToTable('"Message" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.action.press(get_message).wait(3000).release().perform()
                self.page.write("Long pressing on message")
                self.page.writeToTable("Long pressing on message")

            except NoSuchElementException:
                self.page.write('Unable to long press "message"')
                self.page.writeToTable('Unable to long press "message" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("delete_option"))

            if not is_present:
                self.page.writeToTable('"delete_option" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.click_mobile_element(self.current_bed.get("delete_option"))
                self.page.write("delete_option has been found and clicked")
                self.page.writeToTable("delete_option has been found and clicked")
            except NoSuchElementException:
                self.page.write('Unable to find "delete_option"')
                self.page.writeToTable('Unable to find "delete_option" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)
                return False


            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("delete_button"))

            if not is_present:
                self.page.writeToTable('"delete_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.click_mobile_element(self.current_bed.get("delete_button"))
                self.page.write("delete_button has been found and clicked")
                self.page.writeToTable("delete_button has been found and clicked")
            except NoSuchElementException:
                self.page.write('Unable to find "delete_button"')
                self.page.writeToTable('Unable to find "delete_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)

            time.sleep(2)


            is_present = self.page.check_element_present(
                self.current_bed.get("get_message"))

            if  is_present:
                self.page.writeToTable('"Message" is found and '
                                       'not deleted, exiting testcases.',
                                       None, None, self.error)
                return False

            else:
                self.page.write("Message has been deleted")
                self.page.writeToTable("Message has been deleted")

            time.sleep(2)


            self.page.write('=== Rendering of UI screens ended at %s ===' %
                            time.strftime("%H:%M:%S"))
            self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                                   - start_time)))
            self.page.writeTableFooter(' Test case executed successfully in, %d Seconds' % (int(time.time()
                                                                   - start_time)))
        except Exception,e:
            subprocess.Popen('adb shell am force-stop "'+self.app_package+'"', shell=True)
            time.sleep(2)
            subprocess.Popen('adb shell am start -n "'+self.app_package+'/'
                         +self.app_activity+'"', shell=True)
            print "Exception :",str(e)
            time.sleep(2)


    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


    def scrol_up(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)



def suite():
    suite = unittest.TestSuite()
    suite.addTest(VerifyDeleteMessageTopicView('test_verify_delete_message_topicview'))
    return suite


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
