"""Automation Class: execute DAT29 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from appium.webdriver.common.touch_action import TouchAction


class VerifyDMScreen(unittest.TestCase):
    """Implementation of DAT29 testcase to verify the add room."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_dm_screen_DAT29'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_dm_screen(self):
        """Testcase to verify the dm screen."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        self.page.write('=== Testcase DAT29 - Verify DM Screen Started at'
                        ' %s ===' % time.strftime('%H:%M:%S'))
        self.page.writeTableHeader(
            'DAT29 - Verify DM Screen')
        time.sleep(1)

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("search_nav_room"))
        if not is_present:
            self.page.writeToTable('"Find people  " search bar is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find people  " search bar is found.')

        try:
            self.page.set_mobile_text("dyna",
                                      self.current_bed.get("search_nav_room"))
            self.page.write('Typed "dyna"  text into search box ')
        except NoSuchElementException:
            self.page.write('Unable to input text in "Find people  " search '
                            'bar"')
            self.page.writeToTable('Unable to input text in "Find people  " '
                                   'search bar"', None, None, self.error)
            return False

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("user_avatar"))
        if not is_present:
            self.page.writeToTable('"user_avatar"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified user_avatar')
                self.page.writeToTable('Successfully verified user_avatar')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding user_avatar '
                                       , None, None,
                                       self.error)

        time.sleep(2)


        is_present = self.page.check_element_present(
            self.current_bed.get("presence_indicator"))
        if not is_present:
            self.page.writeToTable('"presence_indicator"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified presence_indicator')
                self.page.writeToTable('Successfully verified presence_indicator')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding presence_indicator '
                                       , None, None,
                                       self.error)

        time.sleep(2)


        is_present = self.page.check_element_present(
            self.current_bed.get("user_name"))
        if not is_present:
            self.page.writeToTable('"user_name"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified user_name')
                self.page.writeToTable('Successfully verified user_name')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding user_name '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("user_email"))
        if not is_present:
            self.page.writeToTable('"user_email"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.write('Successfully verified user_email')
                self.page.writeToTable('Successfully verified user_email')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding user_email '
                                       , None, None,
                                       self.error)

        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("search_preview_layout_dat29")
            first_image = self.golden_images + '/search_preview_layout_dat29.png'
            second_image = self.tc_dir_path + '/search_preview_layout_dat29.png'
            search_preview_layout_dat29 = self.images.compare_images(
                first_image, second_image, 'search_preview_layout_dat29', self.tc_dir_path)

            score = float(round(search_preview_layout_dat29, 1))
            if score >= 1:
                self.page.write(
                    'Search Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'search Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/search_preview_layout_dat29'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Search Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Search Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/search_preview_layout_dat29'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'search_preview_layout_dat29')

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("clear_search"))
        if not is_present:
            self.page.writeToTable('"clear_search"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("clear_search"))
                self.page.write('Successfully verified clear_search')
                self.page.writeToTable('Successfully verified clear_search')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding clear_search '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("click_dynamite_1"))
        if not is_present:
            self.page.writeToTable('"click_dynamite_1"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("click_dynamite_1"))
                self.page.write('Successfully verified click_dynamite_1')
                self.page.writeToTable('Successfully verified click_dynamite_1')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding click_dynamite_1 '
                                       , None, None,
                                       self.error)


        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat29")
            first_image = self.golden_images + '/room_preview_layout_dat29.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat29.png'
            room_preview_layout_dat29 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat29', self.tc_dir_path)

            score = float(round(room_preview_layout_dat29, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat29'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat29'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout_dat29')

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("nav_up_button"))
        if not is_present:
            self.page.writeToTable('"nav_up_button"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("nav_up_button"))
                self.page.write('Successfully verified nav_up_button')
                self.page.writeToTable('Successfully verified nav_up_button')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding nav_up_button '
                                       , None, None,
                                       self.error)

        time.sleep(2)



        self.page.write('=== DAT29 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' %
                        (int(time.time() - start_time)))
        self.page.writeTableFooter(' Test case DAT29 executed successfully in,'
                                   ' %d Seconds' % (int(time.time() -
                                                        start_time)))

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyDMScreen('test_verify_dm_screen'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
