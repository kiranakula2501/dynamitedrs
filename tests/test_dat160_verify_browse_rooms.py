"""Automation Class: execute DAT160 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction
from datetime import datetime


class VerifyBrowseRooms(unittest.TestCase):
    """Implementation of DAT160 testcase to verify browse rooms."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + \
                             '/verify_browse_rooms_DAT160'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_browse_rooms(self):

        """Testcase to Create a New Room ."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())

        self.page.write(
            '=== Testcase DAT160 - Verify Browse Rooms Started at %s ===' %
            time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader(
            'DAT160 - Testcase to Verify Browse Rooms')

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable('"browse_rooms" is not found.',
                                   None, None, self.error)

        else:
            self.page.writeToTable('"browse_rooms" is found.')

        try:
            self.page.click_mobile_element(
                self.current_bed.get("browse_rooms"))
            self.page.writeToTable('Successfully clicked on "browse_rooms ".')
            self.page.write('Successfully clicked on browse_rooms')
        except NoSuchElementException:
            self.page.write('Unable to click "browse_rooms"')
            self.page.writeToTable('Unable to click "browse_rooms"',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable('"browse_rooms" title is not found.',
                                   None, None, self.error)

        else:
            self.page.writeToTable('"browse_rooms" title is found.')

        try:
            self.page.write('Successfully found browse_rooms title')
            self.page.writeToTable('Successfully found browse_rooms title.')
        except NoSuchElementException:
            self.page.write('Unable to find browse_rooms title')
            self.page.writeToTable('Unable to click browse_rooms title',
                                   None, None, self.error)

        time.sleep(2)

        self.page.driver.hide_keyboard()

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_browse_rooms_page_dat160")
            first_image = self.golden_images + \
                          '/verify_browse_rooms_page_dat160.png'
            second_image = self.tc_dir_path + \
                           '/verify_browse_rooms_page_dat160.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_browse_rooms_page_dat160', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify Creating room screen verfied successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify Creating room screen verfied successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path +
                    '/verify_browse_rooms_page_dat160' + 'Modified.png')
            else:
                self.page.write(
                    'Verify Creating room screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify Creating room screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path +
                    '/verify_browse_rooms_page_dat160' + 'Modified.png',
                    self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_browse_rooms_page_dat160')

    
        time.sleep(2)
    
        is_present = self.page.check_element_present(
            self.current_bed.get("find_room"))
        if not is_present:
            self.page.writeToTable(
                '"Find room" search bar is not found.',
                None, None, self.error)
            self.page.write('find room search bar is not found')

        else:
            self.page.writeToTable(
                '"Find room" search bar is present on the screen.')

        try:
            self.page.write('Find room search bar is present on screen')

        except NoSuchElementException:
            self.page.writeToTable(
                'Unable to enter text in "find room" search bar"',
                None, None, self.error)

        time.sleep(2)

        self.page.write('=== DAT160 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT160 executed successfully in, %d Seconds' %
            (int(time.time() - start_time)))

    def go_back(self, rotate):
        """function to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for _ in range(rotate):
            self.page.driver.back()

    def scrol_up(self):
        """function to scroll down the screen"""
        scroll_up = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_up[0], scroll_up[1], scroll_up[2],
                               scroll_up[3], scroll_up[4])
        time.sleep(2)


def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyBrowseRooms('test_verify_browse_rooms'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
