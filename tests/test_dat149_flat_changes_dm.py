"""Automation Class: execute DAT149 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException



class VerifyFlatDmChanges(unittest.TestCase):
    """Implementation of DAT149 Testcase to Verify UI changes of DM"""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root \
                             + '/verify_dm_page_DAT149'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_flat_dm_changes(self):

        """ Testcase to Verify UI changes of DM."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        time.sleep(1)

        self.page.write(
            '=== Testcase to Verify UI changes of DM Started at %s ===' %
            time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader(
            'DAT149 - Testcase to Verify UI changes of DM Started')

        self.pull_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("dynaandro",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering Dynamite Account into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("dm_dyna_1"))
        if not is_present:
            self.page.writeToTable('"dm_dyna_1"  is not found.',
                                   None, None, self.error)
            return False
        else:
            try:
                self.page.click_mobile_element(self.current_bed.get("dm_dyna_1"))
                self.page.write('Successfully verified dm_dyna_1')
                self.page.writeToTable('Successfully verified dm_dyna_1')

            except NoSuchElementException as e:
                self.page.writeToTable('Failed on finding dm_dyna_1 '
                                       , None, None,
                                       self.error)

        time.sleep(2)

        self.page.click_mobile_element(self.current_bed.get("dm_dyna_1"))

        self.go_back(1)

        # self.page.driver.hide_keyboard()

        is_present = self.page.check_element_present(
            self.current_bed.get("notification_bell"))
        if is_present:
            self.page.writeToTable(
                '"Notification Bell" Icon is found.',
                None, None, self.error)
        else:
            self.page.write('Notification Bell Icon is Not Found.')
            self.page.writeToTable(
                '"Notification Bell" Icon is Not Found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("new_conversation"))
        if is_present:
            self.page.writeToTable(
                '"New conversation" Button is found.',
                None, None, self.error)
        else:
            self.page.write('New conversation Button is Not Found.')
            self.page.writeToTable(
                '"New conversation" Button is Not Found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("reply_button"))
        if is_present:
            self.page.writeToTable(
                '"Reply Button" is found.',
                None, None, self.error)
        else:
            self.page.write('"Reply Button" is Not Found.')
            self.page.writeToTable(
                '"Reply Button" is Not Found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("compose_message"))
        if not is_present:
            self.page.writeToTable(
                '"input box" is Not found.',
                None, None, self.error)
        else:
            self.page.write(
                '"input box" is found .')
            self.page.writeToTable(
                '"input box" is  Found.')



        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_flat_dm_page_screen")
            first_image = self.golden_images + '/verify_flat_dm_page_screen.png'
            second_image = self.tc_dir_path + '/verify_flat_dm_page_screen.png'
            verify_login = self.images.compare_images(
                first_image, second_image, 'verify_flat_dm_page_screen',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify flat DM Page screen verfied successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify flat DM Page screen verfied successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/verify_flat_dm_page_screen'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Verify flat DM Page screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify flat DM Page screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/verify_flat_dm_page_screen'
                    + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_flat_dm_page_screen')

            self.page.driver.back()

        self.page.write('=== DAT149 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT149 executed successfully in, %d Seconds'
            % (int(time.time() - start_time)))


    def pull_up(self):
        """method to pull up the screen"""
        scroll_up = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_up[0], scroll_up[1], scroll_up[2],
                               scroll_up[3], scroll_up[4])
        time.sleep(2)

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(VerifyFlatDmChanges('test_verify_flat_dm_changes'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
