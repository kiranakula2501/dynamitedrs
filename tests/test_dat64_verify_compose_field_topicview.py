"""Automation Class: execute DAT64 Test Case"""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException



class VerifyComposeTopicView(unittest.TestCase):
    """Implementation of DAT64 testcase to verify compose field topic view."""
    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_compose_field_topicview_DAT64'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'


    def test_verify_compose_topic_view(self):

        """Testcase to validate topic reply view preview screen."""
        try:
            start_time = int(time.time())
            time.sleep(1)

            self.page.write('=== Testcase Verify Compose field topic view Started at %s ===' %
                            time.strftime("%H:%M:%S"))

            self.page.writeTableHeader(
                'DAT64 - Verify Compose Field Topic View Testcase')
            time.sleep(1)
            self.scrol_up()

            is_present = self.page.check_element_present(
                self.current_bed.get("search_box"))
            if not is_present:
                self.page.writeToTable(
                    '"Find room or DM" search bar is not found.',
                    None, None, self.error)
                return False
            else:
                self.page.writeToTable('"Find room or DM" search bar is found.')

            try:
                self.page.set_mobile_text("Dat64 Room",
                                          self.current_bed.get("search_box"))
                self.page.write('Entering "Dat64" Room into search box ')
            except NoSuchElementException:
                self.page.write(
                    'Unable to input text in "Find room or DM" search '
                    'bar"')
                self.page.writeToTable(
                    'Unable to input text in "Find room or DM" '
                    'search bar"', None, None, self.error)
                return False

            time.sleep(2)

            is_present = self.page.check_element_present(
                    self.current_bed.get("dat64_room"))
            if not is_present:
                self.page.writeToTable(
                    '"Dat64" Room is not found.',
                    None, None, self.error)
            else:
                self.page.writeToTable('"Dat64" Room is found.')
                self.page.write("DAT64 Room found")
            try:
                self.page.click_mobile_element(
                    self.current_bed.get("dat64_room"))
            except NoSuchElementException:
                self.page.write('Unable to click on Dat64 Room')
                self.page.writeToTable('Unable to click on Dat64 Room',
                                       None, None, self.error)

            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("reply_to_topic"))
            if not is_present:
                self.page.writeToTable(
                    'Reply topic not found',
                    None, None, self.error)
            else:
                self.page.writeToTable('Reply topic has been verified')
                self.page.write("Reply topic has been verified")
            try:
                self.page.click_mobile_element(
                    self.current_bed.get("reply_to_topic"))
            except NoSuchElementException:
                self.page.write('Unable to click on reply_to_topic')
                self.page.writeToTable('Unable to click on reply_to_topic',
                                       None, None, self.error)

            time.sleep(2)

            self.page.driver.hide_keyboard()

            time.sleep(2)

            self.page.write('Verifying topic reply preview')

            if conf.execution_type == 'automation':
                self.page.save_screenshot_png("topic_preview_layout_dat64")
                first_image = self.golden_images + '/topic_preview_layout_dat64.png'
                second_image = self.tc_dir_path + '/topic_preview_layout_dat64.png'
                topic_preview_layout_dat64 = self.images.compare_images(
                    first_image, second_image, 'topic_preview_layout_dat64', self.tc_dir_path)

                score = float(round(topic_preview_layout_dat64, 1))
                if score >= 1:
                    self.page.write(
                        'Space View Preview Layout screen verified successfully. '
                        'no major changes observed.')
                    self.page.writeToTable(
                        'Room Preview Layout screen verified successfully. '
                        'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/topic_preview_layout_dat64' + 'Modified.png',)
                else:
                    self.page.write(
                        'Space View Preview Layout screen verification failed. '
                        'Major changes observed.', 'CRITICAL')
                    self.page.writeToTable(
                        'Space View Layout screen verification failed. '
                        'Major changes observed.', first_image,
                        self.tc_dir_path + '/topic_preview_layout_dat64'
                        + 'Modified.png', self.error)
            else:
                self.page.golden_image_screenshot(
                    self.golden_images, 'topic_preview_layout_dat64')

            is_present = self.page.check_element_present(
                self.current_bed.get("hint_text"))

            if not is_present:
                self.page.writeToTable('"hint_text" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("hint_text has been found")
                self.page.writeToTable("hint_text has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "hint_text"')
                self.page.writeToTable('Unable to find "hint_text" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)

            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("select_image_button"))

            if not is_present:
                self.page.writeToTable('"select_image_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("select_image_button has been found")
                self.page.writeToTable("select_image_button has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "select_image_button"')
                self.page.writeToTable('Unable to find "select_image_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)

            time.sleep(2)

            is_present = self.page.check_element_present(
                self.current_bed.get("capture_image_button"))

            if not is_present:
                self.page.writeToTable('"capture_image_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("capture_image_button has been found")
                self.page.writeToTable("capture_image_button has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "capture_image_button"')
                self.page.writeToTable('Unable to find "capture_image_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)


            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("create_videocall_button"))

            if not is_present:
                self.page.writeToTable('"create_videocall_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("create_videocall_button has been found")
                self.page.writeToTable("create_videocall_button has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "create_videocall_button"')
                self.page.writeToTable('Unable to find "create_videocall_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)



            time.sleep(2)
            is_present = self.page.check_element_present(
                self.current_bed.get("post_button"))

            if not is_present:
                self.page.writeToTable('"post_button" is not found and '
                                       'exiting all the testcases.',
                                       None, None, self.error)
                return False

            try:
                self.page.write("post_button has been found")
                self.page.writeToTable("post_button has been found")
            except NoSuchElementException:
                self.page.write('Unable to find "post_button"')
                self.page.writeToTable('Unable to find "post_button" ,'
                                       ' exiting all the testcases.', None, None,
                                       self.error)



            time.sleep(2)


            self.page.write('=== Rendering of UI screens ended at %s ===' %
                            time.strftime("%H:%M:%S"))
            self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                                   - start_time)))
            self.page.writeTableFooter(' Test case executed successfully in, %d Seconds' % (int(time.time()
                                                                   - start_time)))
        except Exception,e:
            subprocess.Popen('adb shell am force-stop "'+self.app_package+'"', shell=True)
            time.sleep(2)
            subprocess.Popen('adb shell am start -n "'+self.app_package+'/'
                         +self.app_activity+'"', shell=True)
            print "Exception :",str(e)
            time.sleep(2)


    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


    def scrol_up(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)



def suite():
    suite = unittest.TestSuite()
    suite.addTest(VerifyComposeTopicView('test_verify_compose_topic_view'))
    return suite


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
