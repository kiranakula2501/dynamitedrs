"""Automation Class: execute DAT2 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import json
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import appium
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException

vpoint1 = 'Choose an account should be displayed with all accounts associated with device'
vpoint2 = 'Add account option should also be available'
vpoint3 = "On choosing account 'cancel' and 'ok' buttons should be tappable"


class VerifyOnboardGetStarted(unittest.TestCase):
    """Implementation of DAT2 testcase to verify onboard into dynamite."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_onboard_DAT2'
        self.images = ValidateImages()

    def test_verify_onboard_get_started(self):
        """Testcase to validate onboard scenarios of dynamite chat."""

        test_steps = ['Launch the app', 'On On-boarding screen tap on "Get Started"']

        verifications = {'Able to launch the application': {'status': 0, 'error': '', 'screen_diff': ''},
                         vpoint1: {'status': 0, 'error': '', 'screen_diff': ''},
                         vpoint2: {'status': 0,'error': '', 'screen_diff': ''},
                         vpoint3: {'status': 0, 'error': '', 'screen_diff': ''}}

        print ('=== Testcase DAT2 Verify Onboard Get Started at %s ===' %
                        time.strftime("%H:%M:%S"))

        # ---- Step 1, Launch the app ----- #
        try:
            app_launch = self.page.driver.launch_app()
            if isinstance(app_launch, appium.webdriver.webdriver.WebDriver):
                print "App is successfully launched"
                verifications['Able to launch the application']['status'] = 1
            else:
                print "App is not launched"

            # ----- Screen comparision ------ #
            self.page.save_screenshot_png("onboard_get_started_message_dat2")
            first_image = self.golden_images + \
                          '/onboard_get_started_message_dat2.png'
            second_image = self.tc_dir_path + \
                           '/onboard_get_started_message_dat2.png'
            onboard_get_started_message_dat2 = self.images.compare_images(
                first_image, second_image, 'onboard_get_started_message_dat2',
                self.tc_dir_path)
            score = float(round(onboard_get_started_message_dat2, 1))
            print score
        except Exception as e:
            print "App launch failed"
        time.sleep(2)
        # ---- Step 1, Launch the app, Completed----- #

        # ---- Step 2, 'On On-boarding screen tap on "Get Started" -----#
        try:
            self.page.click_mobile_element(self.current_bed.get("get_started"))

            self.page.save_screenshot_png("onboard_get_started_dat2")
            first_image = self.golden_images + '/onboard_get_started_dat2.png'
            second_image = self.tc_dir_path + '/onboard_get_started_dat2.png'
            onboard_get_started_dat2 = self.images.compare_images(
                first_image, second_image, 'onboard_get_started_dat2',
                self.tc_dir_path)
            score = float(round(onboard_get_started_dat2, 1))

            choose = self.page.check_element_present(self.current_bed.get("choose_account"))
            if not choose:
                verifications[vpoint1]['error'] = "Choose account text is missing"
            else:
                verifications[vpoint1]['status'] = 1

            verifications[vpoint1]['screen_generated'] = \
                self.tc_dir_path + '/onboard_get_started_dat2.png'
            verifications[vpoint1]['screen_diff'] = \
                self.tc_dir_path + '/' + 'onboard_get_started_dat2Modified.png'

            add_acc = self.page.check_element_present(self.current_bed.get("add_account"))
            if not add_acc:
                verifications[vpoint2]['error'] = "Add account text/ Option is missing"
            else:
                verifications[vpoint2]['status'] = 1

            try:
                self.page.click_mobile_element(
                    self.current_bed.get("cancel_button"))
                print ('Clicked on cancel button.')
                verifications[vpoint3]['status'] = 1
            except NoSuchElementException:
                verifications[vpoint3]['error'] = "Cancel button is not tappable"

            self.page.driver.launch_app()
            self.page.click_mobile_element(self.current_bed.get("get_started"))

            try:
                self.page.click_mobile_element(
                    self.current_bed.get("account_to_run"))
                verifications[vpoint3]['status'] = 1
            except NoSuchElementException:
                verifications[vpoint3]['error'] = "Given account is not tappable"

            self.page.click_mobile_element(self.current_bed.get("OK"))
            time.sleep(3)
            self.page.click_mobile_element(self.current_bed.get("allow_button"))

        except Exception as e:
            print 'On On-boarding screen tap on "Get Started" step failed.'

        test_filename = __file__.split("/")[1].split(".")[0]
        with open(self.tc_dir_path + "/" + test_filename + ".json", 'w') as outfile:
            json.dump({'verifications': verifications}, outfile)

        with open(self.tc_dir_path + "/logcat.txt", 'w') as outfile:
            logcat = self.page.driver.get_log('logcat')
            outfile.write(str(logcat))

        with open(self.tc_dir_path + "/server.txt", 'w') as outfile:
            server = self.page.driver.get_log('server')
            outfile.write(str(server))

        print ('=== DAT2 - ended at %s ===' % time.strftime("%H:%M:%S"))


def suite():
    suite = unittest.TestSuite()
    suite.addTest(VerifyOnboardGetStarted('test_verify_onboard_get_started'))
    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
