"""
Mobile crediantials
"""
import os, sys
import yaml

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import argparse
import sys
from appium import webdriver

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CONF_YAML = ROOT_DIR + '/conf/config.yml'

ap = argparse.ArgumentParser()
ap.add_argument("-type", "--execution_type", required=True, help="execution type, 'automation' to validate the UI,"                                                                 " 'capture' to capture the golden images")
ap.add_argument("-acc", "--account", required=True, help="Dynamite account")
ap.add_argument("-d", "--device", required=True, help="Device to execute automation")

args = vars(ap.parse_args())

with open(CONF_YAML, 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

if args.get('device') not in cfg.keys():
    print "Incorrect device name given, available configurations are %s " % cfg.keys()
    sys.exit()

cfg = cfg.get(args.get('device'))

apk_path = ROOT_DIR + '/apks/'

start_activity = 'com.google.android.apps.dynamite.StartUpActivity_Alias'

desired_caps = {}
desired_caps['platformName'] = cfg.get('platformName')
desired_caps['platformVersion'] = cfg.get('platformVersion')
desired_caps['deviceName'] = cfg.get('deviceName')
desired_caps['udid'] = cfg.get('udid')
desired_caps['appPackage'] = cfg.get('appPackage')
desired_caps['appActivity'] = cfg.get('appActivity')
desired_caps['ignoreUnimportantViews'] = 'false'
# desired_caps['automationName'] = cfg.get('automationName')

driver = webdriver.Remote('http://127.0.0.1:' + cfg.get('port') + '/wd/hub', desired_caps)

golden_images_root = ROOT_DIR +'/images/' + cfg.get('images')
screenshots_dir = ROOT_DIR +'/' + cfg.get('screenshots')
reports_dir = ROOT_DIR +'/' + cfg.get('reports')
account = args.get("account")

if cfg.get('mobile_test_bed') == 'Pixel2':
    from locators_conf import Pixel2_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'Pixel':
    from locators_conf import Pixel_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'nexus6':
    from locators_conf import nexus6_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'nexus6p':
    from locators_conf import nexus6p_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'nexus5X':
    from locators_conf import nexus5X_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'PixelXL':
    from locators_conf import PixelXL_test_bed as test_bed
elif cfg.get('mobile_test_bed') == 'SamsungS9':
    from locators_conf import SamsungS9_test_bed as test_bed
else:
    print "invalid test bed given"
    sys.exit()

test_bed['account_to_run'] = "//android.widget.CheckedTextView[contains(@text,'" + account + "')]"
test_bed['gmat_email1'] = account
test_bed['account1'] = "//android.widget.TextView[contains(@content-desc,'Switch to " + account + "')]"

execution_type = args['execution_type']

if not execution_type:
    print "Please enter execution type."
    sys.exit()
elif execution_type not in ['automation', 'capture']:
    print "Invalid execution type given and exiting the execution"
    sys.exit()
else:
    pass

current_test_bed = test_bed
