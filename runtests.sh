#!/usr/bin/env bash

: <<'END_COMMENT'
    Bash script to run the automation. Below explained how to utilise this script.
    Usage:
    chmod u+x runtests.sh
    ./runtests.sh testcasenumber emailaccount device
    ./runtests.sh dat2 dynanadroauto1@dynamitegroup.net Pixel2

END_COMMENT

if [ $# -lt 3 ]
  then
    echo "Please share arguments to execute the script"
    exit $?
fi

rootdir="$(dirname "$(pwd)")"

testfilesdir="$rootdir/dynamite/tests"

array=()

accounts=('dynaandroauto1@dynamitegroup.net' 'dynaandroauto2@dynamitegroup.net' 'dynaandroauto3@dynamitegroup.net')

found=false

for var in "${accounts[@]}"; do
    if [ "$var" = $2 ]; then
        found=true
    fi
done

if [ $found = false ]; then
    echo "Given account '$2' is not found in the preconfigured accounts list.
Ask your manager or team lead for correct accounts"
    exit $?
fi

for file in $testfilesdir/*; do
    filename="$(basename "$file")"
    if [[ $filename = *'.pyc'* ]]; then
      continue
    fi
    IFS='_' read -r -a split <<< "$filename"
    if [ "${split[1]}" = "$1" ];
      then
        python $file --account $2 --execution_type automation --device $3
    fi

done
